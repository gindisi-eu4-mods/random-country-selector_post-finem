on_startup = {
	if = {
		limit = {
			is_mod_active = "Random Country Selector"
			tag = PIR
		}
		log = "on_startup: Random Country Selector for Post Finem v1.0.4 is enabled."
	}
	else_if = {
		limit = { tag = PIR }
		log = "on_startup: WARNING: Random Country Selector for Post Finem is enabled, but base Random Country Selector is not! Both must be enabled." 
	}
}