name="Random Country Selector for Post Finem"
version="1.0.4"
picture="thumbnail.png"
dependencies={
	"Post Finem"
	"Random Country Selector"
}
tags={
	"Gameplay"
	"Utilities"
	"Events"
	"Map"
}
supported_version="v1.37.*.*"
remote_file_id="3160136217"